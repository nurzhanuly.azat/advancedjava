
import DB.Postgres;
import Univeristy.Group;
import Univeristy.Student;

import java.sql.*;

public class Main {
    public static void main(String[] args) {

        Student student = new Student();
        Group group = new Group();

        try (Connection connection = Postgres.connection()){
            Statement statement = connection.createStatement();
            Statement statement1 = connection.createStatement();
            ResultSet resultStudent = statement.executeQuery("SELECT * FROM student");
            ResultSet resultGroup = statement1.executeQuery("SELECT * FROM \"group\"");
            if (resultGroup != null) {
                while(resultGroup.next()) {
                    group.setId(resultGroup.getInt("id"));
                    group.setName(resultGroup.getString("name"));
                    System.out.println(group);
                }
            }
            if (resultStudent != null) {
                while(resultStudent.next()) {
                    student.setId(resultStudent.getInt("id"));
                    student.setGroupid(resultStudent.getInt("groupid"));
                    student.setName(resultStudent.getString("name"));
                    student.setPhone(resultStudent.getString("phone"));
                    System.out.println(student);
                }
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }
}
